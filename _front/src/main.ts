import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { Quasar } from 'quasar'
import App from './App.vue'
import router from './router'
import axios from 'axios'

// Import icon libraries
import '@quasar/extras/material-icons/material-icons.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

axios.defaults.baseURL = 'http://localhost:8000';
axios.defaults.withCredentials = true;

createApp(App)
    .use(createPinia())
    .use(Quasar, {
        plugins: {}, // import Quasar plugins and add here
    })
    .use(router)
    .mount('#app')
