import { createRouter, createWebHistory } from 'vue-router'
import Home from '../pages/Home.vue'
import Questions from '../pages/Questions.vue'
import Login from '../pages/auth/Login.vue'
import Logout from '../pages/auth/Logout.vue'

const routes =  [
    { path: '/', name: 'home', component: Home },
    { path: '/questions', name: 'questions', component: Questions },
    { path: '/login', name: 'login', component: Login },
    { path: '/logout', name: 'logout', component: Logout },
];

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router