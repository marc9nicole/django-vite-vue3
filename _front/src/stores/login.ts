// @ts-check
import { defineStore } from 'pinia'
import { useSessionStorage } from '@vueuse/core'
import { djangoApi } from '../djangoApi'

await djangoApi.get('/api/v1/login-set-cookie/');


export const useLogin = defineStore({
  id: 'login',
  state: () => ({
    user: useSessionStorage('currentUser', {
      username: '',
      firstname: '',
      lastname: '',
      email: '',
      isAuthenticated: false,
      isSuperuser: false,
      rights: { anonymous: true },
    }),
    error: '',
    status: 200
  }),

  actions: {
    async login(password) {
      try {
        const response = await djangoApi.post('/api/v1/login', {
          username: this.user.username,
          password,
        })
        this.error = ''
        console.log(response)
        this.user = response.data.user;
      }
      catch (e) {
        this.status = e.response.status
        this.error = e.toString()
      }
    },

    async logout() {
      try {
        const response = await djangoApi.post('/api/v1/logout');
        this.error = ''
        console.log(response)
        this.user = {username:'anonymous',firstname:'visiteur'};
      }
      catch (e) {
        this.status = e.response.status
        this.error = e.toString()
      }
    }
  },
})

