import json
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST
import logging

logger = logging.getLogger(__name__)

@ensure_csrf_cookie
def login_set_cookie(request):
    """
    `login_view` requires that a csrf cookie be set.
    `getCsrfToken` in `auth.js` uses this cookie to
    make a request to `login_view`
    """
    return JsonResponse({"details": "CSRF cookie set"})

@require_POST
def login_view(request):
    """
    This function logs in the user and returns
    and HttpOnly cookie, the `sessionid` cookie
    """
    data = json.loads(request.body)
    username = data.get("username")
    password = data.get("password")
    if username is None or password is None:
        return JsonResponse(
            {"errors": {"__all__": "Please enter both username and password"}},
            status=400,
        )
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        logger.info('login of %s',username)
        return JsonResponse({
            "detail": "Success",
            "user":{
                "username":request.user.username,
                "email":request.user.email,
                "firstname":request.user.first_name,
                "lastname":request.user.last_name,
                "isSuperuser":request.user.is_superuser,
                "isAuthenticated":request.user.is_authenticated
            }
        })
    return JsonResponse({"detail": "Invalid credentials"}, status=400)

def logout_view(request):
    """
    This function logs out user and returns
    and HttpOnly cookie, the `sessionid` cookie
    """
    logger.info('logout of %s',getattr(request.user,'username','anonymous'))
    logout(request)
    return JsonResponse({"detail":"logout"})