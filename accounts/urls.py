from django.urls import path
#from rest_framework import routers
from . import views

app_name = 'accounts'
urlpatterns = [
    path("login-set-cookie/", views.login_set_cookie, name="login-view"),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout")
]


