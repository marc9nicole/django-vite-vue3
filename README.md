# stack

## Django

comme dans le tuto de django

### djangorestframework

pour écrire facilement l'API et avoir une doc automatique
* aussi CORS managment car le front en dev tourne sur localhost:3000 et en back l'API (comme les pages d'admin) tournent en localhost:8000

## Vite + Vue3

cf https://github.com/MrBin99/django-vite
   https://www.codegrepper.com/code-examples/typescript/vue3%2C+vite+and+django

la solution retenue est d'avoir créé avec `npm init vue@latest` un "projet" spa-front à l'intérieur du projet django
du coup tout se trouve dans un seul projet (+ facile pour gitlab et le déploiement) mais avec 2 projets bien séparé (le front est complétement contenu dans un seul répertoire)

voir aussi le [README.md du front](./spa-front/README.md)

## TODO
* rien n'a été testé pour mettre en prod (vite build + .... )
  * servir les fichiers statiques peut être fait par django, mais en général est (dans Django) délégué au serveur. 
    c'est la manière recommandée par Django, que l'on trouve également sur Python anywhere ...
   