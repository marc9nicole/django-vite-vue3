from .models import Choice, Question
from rest_framework import serializers
from polls.models import Choice, Question


class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ['id','url','question','choice_text','votes']

class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True,read_only=True)
    class Meta:
        model = Question
        fields = ['id','question_text','choices','pub_date']
