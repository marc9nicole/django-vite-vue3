from django.urls import path
from rest_framework import routers

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),
]

# API
api_router = routers.DefaultRouter()
api_router.register(r'questions',views.QuestionsViewSet)
api_router.register(r'choices',views.ChoicesViewSet)

